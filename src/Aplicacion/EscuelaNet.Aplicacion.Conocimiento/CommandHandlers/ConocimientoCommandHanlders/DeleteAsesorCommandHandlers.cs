﻿using EscuelaNet.Aplicacion.Conocimiento.Command.ConocimientoCommand;
using EscuelaNet.Aplicacion.Conocimiento.Responds;
using EscuelaNet.Dominio.Conocimientos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.CommandHandlers.ConocimientoCommandHanlders
{
    public class DeleteAsesorCommandHandlers : IRequestHandler<BorrarAsesorCommand, CommandRespond>
    {
        private ICategoriaRepository _categoriarepositorio;
        public DeleteAsesorCommandHandlers(ICategoriaRepository categoriaRepositorio)
        {
            _categoriarepositorio = categoriaRepositorio;
        }
        public Task<CommandRespond> Handle(BorrarAsesorCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            try
            {
                var conocimientobuscado = _categoriarepositorio.GetConocimieto(request.IDConocimiento);
                var asesorbuscado = conocimientobuscado.Asesores.First(s => s.ID == request.IDAsesor);

                conocimientobuscado.AgregarAsesor(asesorbuscado);

                _categoriarepositorio.Update(conocimientobuscado.Categoria);
                _categoriarepositorio.UnitOfWork.SaveChanges();

                responde.Succes = true;
                return Task.FromResult(responde);

            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = ex.Message;
                return Task.FromResult(responde);
            }
        }

    }
}
