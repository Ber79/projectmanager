﻿using EscuelaNet.Aplicacion.Clientes.QueryModels;
using EscuelaNet.Aplicacion.Clientes.Responds;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.Commands.UnidadCommand
{
    public class UnlinkSolicitudCommand : IRequest<CommandRespond>
    {
        public int IDUnidad { get; set; }

        public int IDSolicitud { get; set; }

        public string RazonSocialUnidad { get; set; }

        public string TituloSolicitud { get; set; }        

    }
}
