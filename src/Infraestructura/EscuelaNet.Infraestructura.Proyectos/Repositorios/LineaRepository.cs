﻿using EscuelaNet.Dominio.Proyectos;
using EscuelaNet.Dominio.SeedWoork;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Proyectos.Repositorios
{
    public class LineaRepository : ILineaRepository
    {
        private LineaContext _contexto;
        public LineaRepository(LineaContext contexto)
        {
            _contexto = contexto;
        }
        public IUnitOfWork UnitOfWork
        {
            get
            {
                return (IUnitOfWork)_contexto;
            }
        }

        public LineaDeProduccion Add(LineaDeProduccion linea)
        {
            _contexto.LineasDeProduccion.Add(linea);
            return linea;
        }
        public void Update(LineaDeProduccion linea)
        {
            _contexto.Entry(linea).State = EntityState.Modified;
        }

        public void Delete(LineaDeProduccion linea)
        {
            _contexto.LineasDeProduccion.Remove(linea);
        }
        public void DeleteProyecto(Proyecto proyecto)
        {
            _contexto.Proyectos.Remove(proyecto);
        }
        public void DeleteEtapa(Etapa etapa)
        {
            _contexto.Etapas.Remove(etapa);
        }
        public LineaDeProduccion GetLinea(int id)
        {
            var linea = _contexto.LineasDeProduccion.Find(id);
            if (linea != null)
            {
                _contexto.Entry(linea)
                    .Collection(l => l.Proyectos).Load();
            }
            return linea;
        }
        public Proyecto GetProyecto(int id)
        {
            var proyecto = _contexto.Proyectos.Find(id);
            if (proyecto != null)
            {
                _contexto.Entry(proyecto)
                    .Reference(d => d.LineaDeProduccion).Load();
                _contexto.Entry(proyecto)
                    .Collection(proy => proy.Etapas).Load();
            }
            return proyecto;
        }
        
        public Etapa GetEtapa(int id)
        {
            var etapa = _contexto.Etapas.Find(id);
            if(etapa != null)
            {
                _contexto.Entry(etapa)
                    .Reference(e => e.Proyecto).Load();
                _contexto.Entry(etapa)
                    .Collection(e => e.Tecnologias).Load();
            }
            return etapa;
        }
        
        public List<LineaDeProduccion> ListLinea()
        {
            return _contexto.LineasDeProduccion.ToList();
        }
    }
}
