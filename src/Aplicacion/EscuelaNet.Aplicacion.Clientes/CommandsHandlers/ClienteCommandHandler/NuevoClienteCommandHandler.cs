﻿using EscuelaNet.Aplicacion.Clientes.Commands.ClienteCommand;
using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.CommandsHandlers.ClienteCommandHandler
{
    public class NuevoClienteCommandHandler : 
        IRequestHandler<NuevoClienteCommand, CommandRespond>
    {
        private IClienteRepository _clienteRepositorio;

        public NuevoClienteCommandHandler(IClienteRepository clienteRepositorio)
        {
            _clienteRepositorio = clienteRepositorio;
        }

        public Task<CommandRespond> Handle(NuevoClienteCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            if (!string.IsNullOrEmpty(request.RazonSocial))
            {
                try
                {
                    _clienteRepositorio.Add(new Cliente(
                            request.RazonSocial, request.Email, request.Categoria
                        ));
                    _clienteRepositorio.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);

                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);
                }

            }
            else
            {
                responde.Succes = false;
                responde.Error = "La Razon Social está vacia.";
                return Task.FromResult(responde);
            }            
        }
    }
}
