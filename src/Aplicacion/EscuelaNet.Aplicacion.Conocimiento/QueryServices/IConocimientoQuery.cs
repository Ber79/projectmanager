﻿using EscuelaNet.Aplicacion.Conocimiento.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.QueryServices
{
    public interface IConocimientoQuery
    {
        ConocimientoQueryModel GetConocimiento(int id);
        List<ConocimientoQueryModel> ListConocimiento(int id);
        List<ConocimientoQueryModel> ListTodasConocimiento();
        List<ConocimientoQueryModel> ListAsesorConocimiento(int id);
        AsesorQueryModel EncontrarAsesor(int id, int conocimiento);


    }
}
