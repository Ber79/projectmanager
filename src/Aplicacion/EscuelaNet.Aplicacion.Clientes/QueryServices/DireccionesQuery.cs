﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using EscuelaNet.Aplicacion.Clientes.QueryModels;

namespace EscuelaNet.Aplicacion.Clientes.QueryServices
{
    public class DireccionesQuery : IDireccionesQuery
    {
        private string _connectionString;

        public DireccionesQuery(string connectionString)
        {
            _connectionString = connectionString;
        }

        public DireccionesQueryModel GetDireccion(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<DireccionesQueryModel>(
                    @"
                    SELECT 
                    d.IDDireccion as ID,
                    d.Domicilio as Domicilio,
                    d.Localidad as Localidad,
                    d.Provincia as Provincia,
                    d.Pais as Pais, 
                    d.IDUnidadDeNegocio as IdUnidadDeNegocio,
                    u.RazonSocial as RazonSocialUnidad
                    FROM Direcciones as d 
                    INNER JOIN UnidadesDeNegocio as u on (d.IDUnidadDeNegocio = u.IDUnidadDeNegocio)  
                    WHERE d.IDDireccion = @id                 
                                                    
                    ", new { id = id }
                    ).FirstOrDefault();
            }
        }

        public List<DireccionesQueryModel> ListDirecciones(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<DireccionesQueryModel>(
                    @"

                    SELECT 
                    d.IDDireccion as ID,
                    d.Domicilio as Domicilio,
                    d.Localidad as Localidad,
                    d.Provincia as Provincia,
                    d.Pais as Pais, 
                    d.IDUnidadDeNegocio as IdUnidadDeNegocio,
                    u.RazonSocial as RazonSocialUnidad,
                    u.IDCliente as IdCliente
                    FROM Direcciones as d 
                    INNER JOIN UnidadesDeNegocio as u on (d.IDUnidadDeNegocio = u.IDUnidadDeNegocio)  
                    WHERE d.IDUnidadDeNegocio = @id                 

                    ", new { id = id }
                    ).ToList();
            }
        }
    }
}
