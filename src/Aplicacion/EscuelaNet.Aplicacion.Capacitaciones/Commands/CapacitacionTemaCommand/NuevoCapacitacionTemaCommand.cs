﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EscuelaNet.Aplicacion.Capacitaciones.QueryModels;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using MediatR;

namespace EscuelaNet.Aplicacion.Capacitaciones.Commands.CapacitacionTemaCommand
{
    public class NuevoCapacitacionTemaCommand : IRequest<CommandRespond>
    {
        public int IDTema { get; set; }
        public int IDCapacitacion { get; set; }
        public string NombreTema { get; set; }

        public List<TemaQueryModel> Temas { get; set; }
    }
}
