﻿using EscuelaNet.Dominio.Conocimientos;
using EscuelaNet.Dominio.SeedWoork;
using EscuelaNet.Infraestructura.Conocimientos.EntityTypeConfigurations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Conocimientos
{
    public class ConocimintosContext : DbContext, IUnitOfWork
    {
        public ConocimintosContext() : base("ConocimintosContext")
        {
        }
        public DbSet<Categoria> Categorias { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CatagoriaEntityTypeConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
